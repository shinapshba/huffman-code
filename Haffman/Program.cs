﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Haffman
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Text:\n> ");
            string input = Console.ReadLine();

            Console.Write(" Input: ");
            PrintBits(GetBits(input));

            // Build the Huffman tree
            HuffmanTree huffmanTree = new HuffmanTree();
            huffmanTree.Build(input);

            // Encode
            BitArray encoded = huffmanTree.Encode(input);
            Console.Write("Result: ");
            PrintBits(encoded);

            // Decode
            //string decoded = huffmanTree.Decode(encoded);
            //Console.WriteLine("Decoded: " + decoded);

            Console.ReadLine();
        }

        static BitArray GetBits(string text)
        {
            var bitsList = new List<bool>();
            foreach(char c in text)
            {
                var byteString = Convert.ToString(c, 2);
                foreach (char bitChar in byteString)
                {
                    if (bitChar.Equals('1'))
                        bitsList.Add(true);
                    else
                        bitsList.Add(false);
                }
            }
            return new BitArray(bitsList.ToArray());
        }

        static void PrintBits(BitArray bits)
        {
            foreach (bool bit in bits)
                Console.Write(bit ? "1" : "0");
            Console.WriteLine();
        }
    }
}